<?php

declare (strict_types = 1);

namespace BlamelessWeb\Google\Services;

use ReCaptcha;

final class CaptchaService
{
    /** @var object */
    private $config;

    public function __construct(object $config)
    {
        $this->config = $config;
    }

    public function verifyResponse($response): bool
    {
        if (isset($response)) {
            $recaptcha = new ReCaptcha\ReCaptcha($this->config->secretKey, new ReCaptcha\RequestMethod\CurlPost());

            $resp = $recaptcha->verify($response, $_SERVER['REMOTE_ADDR']);

            return $resp->isSuccess();
        }
        return false;
    }

    public function getConfig(): object
    {
        return $this->config;
    }
}
