<?php

declare (strict_types = 1);

namespace BlamelessWeb\Google\Services;

final class AnalyticsService
{
    /** @var object */
    private $config;

    public function __construct(object $config)
    {
        $this->config = $config;
    }

    public function getConfig(): object
    {
        return $this->config;
    }
}
