<?php

declare (strict_types = 1);

namespace BlamelessWeb\Google\Components\Factories;

use BlamelessWeb\Google\Components;

interface IGoogleTagFactory
{
    function create(): Components\GoogleTag;
}
