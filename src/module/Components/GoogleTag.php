<?php

declare (strict_types = 1);

namespace BlamelessWeb\Google\Components;

use Nette\Application\UI\Control;

final class GoogleTag extends Control
{
    /** @var ?object */
    private $config;

    public function __construct(?object $config)
    {
        $this->config = $config;
    }

    public function render(): void
    {
        $this->template->config = $this->config;
        $this->template->render(__DIR__ . '/templates/gTag.latte');
    }
}
