<?php

declare (strict_types = 1);

namespace BlamelessWeb;

use BlamelessWeb\Google\Components;
use BlamelessWeb\Google\Services;
use Nette;
use Nette\Schema\Expect;

class GoogleExtension extends Nette\DI\CompilerExtension
{
    public function getConfigSchema(): Nette\Schema\Schema
    {
        return Expect::structure([
            'captcha' => Expect::structure([
                'siteKey' => Expect::string(),
                'secretKey' => Expect::string(),
            ]),
            'tags' => Expect::structure([
                'analytics' => Expect::structure([
                    'id' => Expect::string(),
                ]),
                'gtm' => Expect::structure([
                    'id' => Expect::string(),
                ]),
            ]),
        ]);
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $builder
            ->addDefinition($this->prefix('captchaService'))
            ->setFactory(Services\CaptchaService::class, [$this->config->captcha]);

        $builder
            ->addDefinition($this->prefix('analyticsService'))
            ->setFactory(Services\AnalyticsService::class, [$this->config->tags]);

        $builder
            ->addFactoryDefinition('googleTagFactory')
            ->setImplement(Components\Factories\IGoogleTagFactory::class)
            ->getResultDefinition()
            ->setArguments([
                $this->config->tags,
            ]);
    }
}
